from django.contrib import admin
from .models import History, Poster


class HistoryAdmin(admin.ModelAdmin):
    list_display = ('search_text', 'created_at')


class PosterAdmin(admin.ModelAdmin):
    list_display = ('title', 'image')


admin.site.register(History, HistoryAdmin)
admin.site.register(Poster, PosterAdmin)