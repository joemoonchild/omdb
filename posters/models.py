from django.db import models
from django.utils.translation import ugettext_lazy as _
from easy_thumbnails.fields import ThumbnailerImageField
from django.template.defaultfilters import slugify



def photo_upload_to(instance, filename):
    return "posters/photo/%s/%s" % (instance.title, filename)


class Poster(models.Model):
    class Meta:
        verbose_name = _(u'Poster')
        verbose_name_plural = _(u'Posters')
        db_table = 'Posters'

    title = models.CharField(_(u'Title'), max_length=128, blank=False, null=False, db_index=True, unique=True)
    image = ThumbnailerImageField('img', upload_to=photo_upload_to, null=True)
    url = models.URLField(_(u'url'), null=True, blank=True)

    def save(self, **kwargs):
        self.title = slugify(self.title)
        models.Model.save(self, **kwargs)



class History(models.Model):
    class Meta:
        verbose_name = _(u'History')
        verbose_name_plural = _(u'History')
        db_table = 'History'

    search_text = models.CharField(_(u'Search Field'), max_length=128, blank=False, null=False)
    created_at = models.DateTimeField(_(u'Created at'), auto_now_add=True)