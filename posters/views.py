from django.shortcuts import render, render_to_response
from django.http import HttpResponse
from django.template.context import RequestContext
from django.core.files.base import File
from django.core.files.temp import NamedTemporaryFile
from django.template.defaultfilters import slugify


import urllib
import urllib2
import xml.etree.ElementTree as ET

from .forms import SearchForm
from .models import Poster


def get_image(poster, poster_link):
    temp_img = NamedTemporaryFile(delete=True)
    temp_img.write(urllib2.urlopen(poster_link).read())
    temp_img.flush()
    extension = poster_link.split('.')
    image_name = poster.title + '.' + extension[-1]
    poster.image.save(image_name, File(temp_img))
    poster.save()


def search(request):
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            form.save()
            api = 'http://www.omdbapi.com/?t='
            additional_params = '&plot=full&r=xml'
            movie_title = form.cleaned_data['search_text'].encode('utf-8')
            feed = urllib.urlopen(api + movie_title + additional_params)
            xml = ET.fromstring(feed.read())

            if xml.attrib['response'] == 'True':
                poster_link = xml[0].attrib['poster']
                if poster_link != 'N/A':
                    poster, _new = Poster.objects.get_or_create(title=slugify(movie_title))
                    if _new:
                        poster.url = poster_link
                        get_image(poster, poster_link)

                    else:
                        if poster.url != poster_link:
                            get_image(poster, poster_link)
                else:
                    return HttpResponse("Sorry no poster for your movie", content_type="text")
            else:
                return HttpResponse("Sorry no results for your search", content_type="text")

        with open(poster.image.url, "rb") as f:
            return HttpResponse(f.read(), content_type="image/jpeg")

    else:
        form = SearchForm()

    return render(request, 'search.html', {'form': form})