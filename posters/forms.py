from django import forms
from .models import History


class SearchForm(forms.ModelForm):
    class Meta:
        model = History
        exclude = ('created_at',)